/*------------------------------------------------------------------------
|                            FILE DESCRIPTION                            |
------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
|  - File name     : os_port.C
|  - Author        : zevorn
|  - Update date   : 2021.03.25
|  - Copyright(C)  : 2021-2021 zevorn. All rights reserved.
------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
|                            COPYRIGHT NOTICE                            |
------------------------------------------------------------------------*/
/*
 * Copyright (C) 2021, zevorn (zevorn@yeah.net)

 * This file is part of Ant Real Time Operating System.

 * Ant Real Time Operating System is free software: you can redistribute 
 * it and/or modify it under the terms of the Apache-2.0 License.
 
 * Ant Real Time Operating System is distributed in the hope that it will 
 * be useful,but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * Apache-2.0 License License for more details.
 
 * You should have received a copy of the Apache-2.0 License.Ant Real Time
 * Operating System. If not, see <http://www.apache.org/licenses/>.
**/
/*------------------------------------------------------------------------
|                                INCLUDES                                |
------------------------------------------------------------------------*/

#include "os_cpu.h"
#include "os_thread.h"
#include "os_core.h"

/*------------------------------------------------------------------------
|                                  DATA                                  |
------------------------------------------------------------------------*/

/**
 * @brief A counter for the number of times the critical section is locked.
 * @note Stored in the data area, the fastest reading and writing speed.
**/
os_uint8_t data g_lock_cnt = 0; 

/**
 * @brief The thread node that is running.
**/
extern os_thread_t *g_thrd_run_node;

/**
 * @brief Thread ready linked list.
**/
extern struct os_thread_list g_thrd_rdylist;

/**
  * @brief RTOS kernel stack.
***/
extern struct os_kernel_stk volatile idata kernel_stack;

/**
 * @brief RTOS access external RAM pointer.
**/
extern os_uint8_t xdata *data g_pxram_stk; 

/**
 * @brief RTOS accesses the internal RAM pointer..
**/
extern os_uint8_t idata *data g_pram_stk; 

/*------------------------------------------------------------------------
|                              API FUNCTIONS                             |
------------------------------------------------------------------------*/

extern os_thread_t os_rdylist_get_max_prio(os_uint8_t *max_prio_label);
extern void os_rdylist_replace(os_uint8_t index1,os_uint8_t index2);
	
/**
 * @brief   Copy the core stack (RAM) to the thread stack (XRAM).
**/
#define COPY_STACK_TO_XRAM() \
do{ \
	g_pxram_stk = (*g_thrd_run_node)->stk_addr; \
	g_pram_stk = kernel_stack.block; \
    kernel_stack.length = SP - (os_uint8_t)kernel_stack.block - 1; \
	*g_pxram_stk = kernel_stack.length; \
	while (kernel_stack.length--) \
	{ \
		*(++g_pxram_stk) = *(++g_pram_stk); \
	} \
}while(0)


/**
 * @brief  Copy the thread stack (XRAM) to the core stack (RAM).
**/
#define COPY_XRAM_TO_STACK() \
do{ \
	g_pxram_stk = (*g_thrd_run_node)->stk_addr; \
	g_pram_stk = kernel_stack.block; \
	kernel_stack.length = g_pxram_stk[0]; \
	while (kernel_stack.length--) \
	{ \
		*(++g_pram_stk) = *(++g_pxram_stk); \
	} \
    SP = (os_uint8_t)(g_pram_stk); \
}while(0)

/**
 * @brief     OS starts system scheduling and selects the highest priority task for execution.
 * @param[in] None.
 * @return    None.
**/
void os_scheduler_init(void)
{	
	os_uint8_t index;

	g_thrd_run_node = (os_thread_t *)os_rdylist_get_max_prio(&index);
    os_rdylist_replace(index,--g_thrd_rdylist.num);
	(*g_thrd_run_node)->status = OS_THREAD_RUNNING;
    
	COPY_XRAM_TO_STACK();

    __asm POP AR3
    __asm POP AR2
    __asm POP AR1
    __asm POP AR7
    __asm POP AR6
    __asm POP AR5
    __asm POP AR4
    __asm POP AR0
    __asm POP PSW
    __asm POP DPL
    __asm POP DPH
    __asm POP B
    __asm POP ACC

	OS_EXIT_CRITICAL();
}


/**
 * @brief RTOS Context Schedule.
 * @param[in] none
 * @return none
 * @attention this is a atomic operation, 
 * You need to provide critical protection if you want to call it
 * @example 
 * OS_ENTER_CRITICAL();
 * os_thread_schedule();
 * OS_EXIT_CRITICAL();
**/
void os_thread_schedule(void)
{
    extern void os_update_list(os_thread_t *list);
	os_uint8_t index;

    __asm PUSH  ACC
    __asm PUSH  B
    __asm PUSH  DPH
    __asm PUSH  DPL
    __asm PUSH  PSW
    __asm PUSH  AR0
    __asm PUSH  AR4
    __asm PUSH  AR5
    __asm PUSH  AR6
    __asm PUSH  AR7
    __asm PUSH  AR1
    __asm PUSH  AR2
    __asm PUSH  AR3
    __asm PUSH  SP
    
    COPY_STACK_TO_XRAM();
    os_update_list(g_thrd_run_node);
	g_thrd_run_node = (os_thread_t *)os_rdylist_get_max_prio(&index);
    os_rdylist_replace(index,--g_thrd_rdylist.num);
	(*g_thrd_run_node)->status = OS_THREAD_RUNNING;
	COPY_XRAM_TO_STACK();
	
    __asm POP AR3
    __asm POP AR2
    __asm POP AR1
    __asm POP AR7
    __asm POP AR6
    __asm POP AR5
    __asm POP AR4
    __asm POP AR0
    __asm POP PSW
    __asm POP DPL
    __asm POP DPH
    __asm POP B
    __asm POP ACC

}


/*------------------------------------------------------------------------
|                    END OF FLIE.  (C) COPYRIGHT zevorn                  |
------------------------------------------------------------------------*/
